import java.util.Arrays;

/**
 * Totally not working program at the moment...
 *
 * Trying to write methods for every task to then use the methods to check if a card number i valid.
 */
public class Main {
    public static void main(String[] args) {
        isValid("12345678987654321");
        System.out.println(isValid("12345678987654321"));
    }

    public static boolean isValid(String credNumInput) {
        int credNum = 0;
        char checkDigitChar = credNumInput.charAt(credNumInput.length()-1);
        String checkDigitString = "" + checkDigitChar;
        int checkDigit = 0;

        if (credNumInput.length() >= 14 && credNumInput.length() <= 19)
        {
            try {
                credNum = Integer.parseInt(credNumInput);
                checkDigit = Integer.parseInt(checkDigitString);
            }
            catch (NumberFormatException e)
            {
                System.out.println("\"" + credNumInput + "\"" + " is not a valid credit card number!");
            }
            System.out.println("CredNum: " + credNum);
            System.out.println("CheckDigit: " + checkDigit);
        }
        else {
            return false;
        }

        //credNumInput = ;


        return false;
    }

    // Return the length of the string
    public static int lengthCheck(long num){
        String numString = num + "";
        return numString.length();
    }

    public static int checkDigit(long num){
        int checkDigit;
        String numString = num + "";
        String lastDigitString = numString.substring(numString.length()-1);
        checkDigit = Integer.parseInt(lastDigitString);
        return checkDigit;
    }

    public static long reverseNumber(long num){
        long reversed;
        String numString = num + "";
        StringBuilder sb = new StringBuilder();
        sb.append(numString);
        sb.reverse();
        String sbString = sb.toString();
        reversed = Long.parseLong(sbString);
        return reversed;
    }


}

